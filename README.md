# (2048 - Ruby - Event Sourcing) Project

A clone of 2048 written in Ruby using the Event Sourcing pattern.
Each move produces a list of events (merges, moves, spawns, etc.).
Those events can be applied to a particular view to get the state at
a point in time.

This project currently consists of the following parts:
  - core: The game-logic (events generator)
  - cliapp: A CLI-based user interface for the game
  - gui: A GUI-based user interface utilizing ruby2D library for graphics

## Install dependecies

Ubuntu-based system:
```
sudo apt-get install libsdl2-dev libsdl2-image-dev libsdl-mixer-dev libsdl-ttf-dev
wget https://raw.githubusercontent.com/simple2d/simple2d/master/bin/simple2d.sh
bash simple2d.sh

```

## Getting started

```
git clone https://gitlab.com/rfmind/2048-ruby-eventsourcing.git
cd 2048-ruby-eventsourcing
bundle install
rake test_core   # run tests
rake run_cliapp  # run curses-based version
rake run_gui     # run ruby2d-based version
```

