require_relative '../core/handlers.rb'
require_relative '../core/events.rb'
require_relative '../core/board.rb'

require 'io/console'
require 'curses'
require 'tty-table'

class Game

    def initialize
        Curses.noecho
        Curses.init_screen

        @window = Curses::Window.new(24,80,0,0)

        events = create_board(4,4)
        @board = nil
        for ev in events
            if @board == nil
                @board = Board.new(ev)
            else
                @board.apply(ev)
            end
        end

        update_view
    end

    def start
        until (chr = @window.getch) == 'q' do
            case
            when chr == 'i' && @board.is_active
                events = move(@board, 'up')
            when chr == 'k' && @board.is_active
                events = move(@board, 'down')
            when chr == 'j' && @board.is_active
                events = move(@board, 'left')
            when chr == 'l' && @board.is_active
                events = move(@board, 'right')
            when chr == 'n' && @board.is_active == false
                events = create_board(4,4)
                @board = Board.new(events.shift)
            else
                next
            end

            for event in events
                @board.apply(event)
                if @board.is_active == false then
                    puts "Game lost"
                    @window.clear
                    @window.addstr("\n Game lost!\n")
                    @window.addstr(" ... press n to start a new one\n")
                    @window.addstr(" ... press q to quit")

                    @window.refresh
                    next
                end
            end

            if @board.is_active then update_view end
        end
    end

    def update_view
        table = TTY::Table.new(rows: @board.grid)
        @window.clear
        @window.addstr("\n  2048 - Ruby - Event Sourcing\n\n  i=up, k=down, j=left, l=right\n\n")
        @window.addstr(table.render(
            :ascii,
            alignment: [:center],
            border: { separator: :each_row },
            padding: [1,1,1,1],
            column_widths: Array.new(@board.grid.length, 5)))
        @window.refresh
    end
end

Game.new.start

