
def merge(grid, vertical=false, reverse=false)

    actions_done = []
    copy = deepcopy(grid, vertical)
    outer_range, inner_range = ranges(copy.length, copy[0].length, reverse)

    for i in outer_range

        to = inner_range.first + (reverse ? 1 : -1)
        for from in inner_range

            from_value = copy[i][from]
            to_value = copy[i][to]

            if from_value != 0
                if to_value == from_value
                    copy[i][to] = to_value + from_value
                    copy[i][from] = 0
                    merge = pair(i, from, i, to, vertical).push(:merge)
                    actions_done.push(merge)
                    to += reverse ? -1 : 1
                    next
                elsif to_value != 0
                    to += reverse ? -1 : 1
                    if to == from
                        next
                    end
                end

                copy[i][to], copy[i][from] = copy[i][from], 0
                move = pair(i, from, i, to, vertical).push(:move)
                actions_done.push(move)
            end
        end
    end

    return actions_done
end

def deepcopy(grid, vertical)
    copy = Array.new(grid.length) { Array.new(grid[0].length) }

    for i in 0..grid.length-1
        for j in 0..grid[i].length-1
            copy[i][j] = vertical ? grid[j][i] : grid[i][j]
        end
    end

    return copy
end


def pair(fromi, fromj, toi, toj, vertical)
    pair = [[fromi, fromj], [toi, toj]]
    if vertical then
        pair = [[fromj, fromi], [toj, toi]]
    end
    return pair
end

def ranges(outer_max, inner_max, reverse)
    outer_range, inner_range = (0).upto(outer_max-1), (1).upto(inner_max-1)

    if reverse then
        outer_range, inner_range = (outer_max-1).downto(0), (inner_max-2).downto(0)
    end

    return outer_range, inner_range
end

