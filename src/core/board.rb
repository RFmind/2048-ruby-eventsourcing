require_relative 'events'

class Board

    def initialize(board_created=nil, from_board=nil)
        if board_created != nil then
            apply_created(board_created)
        else
            @id = from_board.id
            @created_at = from_board.created_at
            @version = from_board.version
            @width = from_board.width
            @height = from_board.height
            @grid = Marshal.load(Marshal.dump(from_board.grid))
            @active = from_board.is_active
        end
    end

    def apply_all(events)
        for ev in events
            apply(ev)
        end
    end

    def apply(event)
        if event.aggregate_id == @id then
            @version = event.aggregate_version
            case
            when event.is_a?(CellMoved)
                apply_cell_moved(event)
            when event.is_a?(CellSpawned)
                apply_cell_spawned(event)
            when event.is_a?(CellsMerged)
                apply_cells_merged(event)
            when event.is_a?(BoardDeactivated)
                apply_deactivated(event)
            end
        end
    end

    def apply_created(event)
        @id = event.aggregate_id
        @created_at = event.timestamp
        @version = event.aggregate_version
        @width = event.payload[:width]
        @height = event.payload[:height]
        @grid = Array.new(@width) { Array.new @height, 0 }
        @active = true
    end

    def apply_cell_spawned(event)
        x = event.payload[:x]
        y = event.payload[:y]
        value = event.payload[:value]
        @grid[x][y] = value
    end

    def apply_cell_moved(event)
        x1 = event.payload[:x1]
        y1 = event.payload[:y1]
        x2 = event.payload[:x2]
        y2 = event.payload[:y2]

        current = @grid[x1][y1]
        old = @grid[x2][y2]
        @grid[x2][y2] = current
        @grid[x1][y1] = 0
    end

    def apply_cells_merged(event)
        x1 = event.payload[:x1]
        y1 = event.payload[:y1]
        x2 = event.payload[:x2]
        y2 = event.payload[:y2]

        @grid[x2][y2] += @grid[x1][y1]
        @grid[x1][y1] = 0
    end

    def apply_deactivated(event)
        @active = false
    end

    def empty_cells
        cells = []
        for x in 0..@grid.length-1
            for y in 0..@grid[x].length-1
                if @grid[x][y] == 0
                    cells.push([x,y])
                end
            end
        end
        return cells
    end

    def has_equal_neighbors?
        for i in 0..@grid.length-2
            for j in 0..@grid[i].length-2
                if @grid[i][j] == @grid[i][j+1] || @grid[i][j] == @grid[i+1][j]
                    return true
                end
            end
        end
        for i in 0..@grid.last.length-2
            if @grid.last[i] == @grid.last[i+1]
                return true
            end
        end
        return false
    end

    def id
        return @id
    end

    def created_at
        return @created_at
    end

    def version
        return @version
    end

    def width
        return @width
    end

    def height
        return @height
    end

    def grid
        return @grid
    end

    def is_active
        return @active
    end
end
