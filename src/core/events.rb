require 'securerandom'

class Event
    def initialize(payload, aggregate_id, aggregate_version)
        @eventid = SecureRandom.uuid
        @aggregate_id = aggregate_id
        @aggregate_version = aggregate_version
        @timestamp = Time.now.getutc
        @payload = payload
    end

    def eventid
        return @eventid
    end

    def payload
        return @payload
    end

    def aggregate_id
        return @aggregate_id
    end

    def aggregate_version
        return @aggregate_version
    end

    def timestamp
        return @timestamp
    end
end

class BoardCreated < Event
    def initialize(width, height, aggregate_id, aggregate_version)
        payload = {
            :width => width,
            :height => height }
        super(payload, aggregate_id, aggregate_version)
    end
end

class CellSpawned < Event
    def initialize(x, y, value, aggregate_id, aggregate_version)
        payload = { :x => x, :y => y, :value => value }
        super(payload, aggregate_id, aggregate_version)
    end
end

class CellMoved < Event
    def initialize(x1, y1, x2, y2, aggregate_id, aggregate_version)
        payload = {
            :x1 => x1, :y1 => y1,
            :x2 => x2, :y2 => y2 }
        super(payload, aggregate_id, aggregate_version)
    end
end

class CellsMerged < Event
    def initialize(x1, y1, x2, y2, aggregate_id, aggregate_version)
        payload = {
            :x1 => x1, :y1 => y1,
            :x2 => x2, :y2 => y2 }
        super(payload, aggregate_id, aggregate_version)
    end
end

class BoardDeactivated < Event
    def initialize(aggregate_id, aggregate_version)
        super(nil, aggregate_id, aggregate_version)
    end
end
