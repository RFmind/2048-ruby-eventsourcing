require 'securerandom'
require_relative './board.rb'
require_relative './events.rb'
require_relative './merge.rb'

def create_board(width, height)
    id = SecureRandom.uuid
    board_created = BoardCreated.new(width, height, id, 0)

    cells = []
    for x in 0..width-1
        for y in 0..height-1
            cells.push([x, y])
        end
    end

    x1, y1 = cells.sample
    cells.delete([x1, y1])

    x2, y2 = cells.sample
    value1 = rand < 0.9 ? 2 : 4
    value2 = rand < 0.9 ? 2 : 4

    spawned1 = CellSpawned.new x1, y1, value1, id, 1 
    spawned2 = CellSpawned.new x2, y2, value2, id, 2 

    return [board_created, spawned1, spawned2]
end

def move(board, direction)
    direction = direction.downcase
    aggregate_version = board.version

    vertical = false
    reverse = false

    if ["up", "down"].include? direction then
        vertical = true
    end

    if ["right", "down"].include? direction then
        reverse = true
    end

    actions = merge(board.grid, vertical, reverse)
    events = []

    actions.each do |action|
        aggregate_version += 1
        event = nil

        if action[2] == :move
            event = CellMoved.new(
                action[0][0], action[0][1], action[1][0], action[1][1],
                board.id, aggregate_version)
        elsif action[2] == :merge
            event = CellsMerged.new(
                action[0][0], action[0][1], action[1][0], action[1][1],
                board.id, aggregate_version)
        end
        events.push(event)
    end

    new_board = Board.new(nil, board)
    for ev in events do new_board.apply(ev) end

    empty_cells = new_board.empty_cells
    x,y = empty_cells.sample
    value = rand < 0.9 ? 2 : 4

    can_spawn = x != nil && y != nil
    if can_spawn && events.length > 0
        aggregate_version += 1
        spawned_event = CellSpawned.new(x, y, value, board.id, aggregate_version)
        events.push(spawned_event)
        new_board.apply(spawned_event)
    elsif !can_spawn
        events.push(BoardDeactivated.new(board.id, aggregate_version+1))
        return events
    end

    has_empty_cells = new_board.empty_cells.length > 0
    move_possible = has_empty_cells || new_board.has_equal_neighbors?
    
    if !move_possible
        events.push(BoardDeactivated.new(board.id, aggregate_version+1))
    end

    return events
end
