
APP_CONFIG = {
    :title => '2048 - Ruby - Event Sourcing',
    :colors => {
        0     => '#666666',
        2     => '#b7931d',
        4     => '#4c9e19',
        8     => '#ed6525',
        16    => '#135796',
        32    => '#77150e',
        64    => '#b7931d',
        128   => '#4c9e19',
        256   => '#ed6525',
        512   => '#135796',
        1024  => '#77150e',
        2048  => '#1a9373',
        4096  => '#1a9373',
        8192  => '#1a9373',
        16384 => '#1a9373'
    },
    :font_path => "src/gui/fonts/FiraSans-SemiBold.ttf",
    :board => {
        :width => 5,
        :height => 5
    },
    :window => {
        :width => 720,
        :height => 840
    },
    :keys => {
        'up' => 'i',
        'down' => 'k',
        'left' => 'j',
        'right' => 'l',
        'exit' => 'e',
        'newgame' => 'n'
    }
}
