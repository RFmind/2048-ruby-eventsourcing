require 'ruby2d'
require_relative '../core/handlers'
require_relative '../core/events'
require_relative '../core/board'
require_relative './config.rb'

class Header

    def initialize(titl, font_path)
        @title = Text.new(
            titl,
            font: font_path,
            color: '#666666',
            size: 30,
            x: 20, y: 20, z: 1)
    end
end

class Info

    def initialize(x, y, font_path)
        @moves = 0
        @score = 0
        txt = "MOVES: 0  SCORE: 0"
        @text = Text.new(
            txt, x: x, y: y,
            color: '#666666',
            font: font_path)
    end

    def add_move
        @moves += 1
        @text.text = "MOVES: #{@moves}  SCORE: #{@score}"
    end

    def add_to_score(amount)
        @score += score
        @text.text = "MOVES: #{@moves}  SCORE: #{@score}"
    end
end

class TileBoard

    def initialize(conf)
        @conf = conf
        @tiles = []
        @active = true

        @margin_left = 20
        @margin_top = 70

        @height = (@conf[:board][:height] * 140) + @margin_top
        @width = (@conf[:board][:width] * 140) + @margin_left

        for x in 0..@conf[:board][:height]-1
            row = []
            for y in 0..conf[:board][:width]-1
                realx = (x * 140) + @margin_left
                realy = (y * 140) + @margin_top
                tile = Tile.new(
                    realx, realy,
                    0, conf[:colors], conf[:font_path])
                row.push(tile)
            end
            @tiles.push(row)
        end
    end

    def height
        return @height
    end

    def apply(events)
        for ev in events
            case
            when ev.is_a?(CellsMerged)
                merge_cells(ev)
            when ev.is_a?(CellMoved)
                move_cell(ev)
            when ev.is_a?(CellSpawned)
                spawn_cell(ev)
            when ev.is_a?(BoardDeactivated)
                @active = false
                end_game(
                    @conf[:window][:width],
                    @conf[:window][:height])
            end
        end
    end

    def move_cell(event)
        tile_from = @tiles[event.payload[:y1]][event.payload[:x1]]
        tile_to = @tiles[event.payload[:y2]][event.payload[:x2]]

        tile_to.set_value(tile_from.get_value)
        tile_from.set_value(0)
    end

    def merge_cells(event)
        tile_from = @tiles[event.payload[:y1]][event.payload[:x1]]
        tile_to = @tiles[event.payload[:y2]][event.payload[:x2]]
        value = tile_from.get_value * 2

        tile_to.set_value(value)
        tile_from.set_value(0)
    end

    def spawn_cell(event)
        tile = @tiles[event.payload[:y]][event.payload[:x]]
        value = event.payload[:value]

        tile.set_value(value)
    end

    def end_game(width, height)
        for row in @tiles
            for tile in row
                tile.remove
            end
        end

        Text.new(
            "TRY AGAIN?",
            x: (width/2)-135, y: (height/2)-25,
            size: 50, font: @conf[:font_path])
    end

    def active
        return @active
    end
end

class Tile

    def initialize(x, y, value, colors, font_path)
        @colors = colors
        @tile = Square.new(
            x: x, y: y,
            size: 120,
            color: @colors[value],
            z: 1)
        @text = Text.new(
            value.to_s,
            color: 'black',
            font: font_path,
            size: 40,
            x: x+20, y: y+20,
            z: 2)
        @value = value
        set_value(value)
    end

    def get_value
        return @value
    end

    def set_value(value)
        @value = value
        @tile.color = @colors[value]
        @text.text = value != 0 ? value.to_s : ''
    end

    def remove
        @tile.remove
        @text.remove
    end
end

class Game

    def initialize(conf)

        @conf = conf

        initialize_window
        initialize_header
        initialize_board
        initialize_footer

        start_game
    end

    def initialize_window
        @window = Window.new
        @window.set(title: APP_CONFIG[:title])
        @window.set(background: 'black')
        @window.set(width: APP_CONFIG[:window][:width])
        @window.set(height: APP_CONFIG[:window][:height])
    end

    def initialize_header
        @header = Header.new(
            @conf[:title],
            @conf[:font_path])
    end

    def initialize_board
        events = create_board(
            @conf[:board][:width],
            @conf[:board][:height])
        @board = Board.new(events.shift)

        @tileboard = TileBoard.new(@conf)
        @tileboard.apply(events)
        @board.apply_all(events)
    end

    def initialize_footer
        @footer = Info.new(
            20, @conf[:window][:height]-40,
            @conf[:font_path])
    end

    def start_game
        @window.on :key_down do |event|
            case event.key
            when @conf[:keys]['up']
                events = move(@board, 'up')
                @tileboard.apply(events)
                @board.apply_all(events)
                if @tileboard.active then @footer.add_move end

            when @conf[:keys]['down']
                events = move(@board, 'down')
                @tileboard.apply(events)
                @board.apply_all(events)
                if @tileboard.active then @footer.add_move end

            when @conf[:keys]['left']
                events = move(@board, 'left')
                @tileboard.apply(events)
                @board.apply_all(events)
                if @tileboard.active then @footer.add_move end

            when @conf[:keys]['right']
                events = move(@board, 'right')
                @tileboard.apply(events)
                @board.apply_all(events)
                if @tileboard.active then @footer.add_move end

            when @conf[:keys]['exit']
                @window.close

            when @conf[:keys]['newgame']
                if !@tileboard.active then
                    @window.clear
                    initialize_header
                    initialize_board
                    initialize_footer
                end
            end
        end

        @window.show
    end
end

game = Game.new(APP_CONFIG)
