require 'test/unit'
require_relative '../../src/core/board.rb'
require_relative '../../src/core/events.rb'

class TestBoard < Test::Unit::TestCase

    def test_apply_created
        event = BoardCreated.new(5,5,2202,0)
        board = Board.new(board_created=event)

        assert_equal 5, board.width
        assert_equal 5, board.height
        assert_equal 2202, board.id
        assert_equal 0, board.version
    end

    def test_apply_cell_spawned
        event = BoardCreated.new(5,5,2202,0)
        board = Board.new(board_created=event)

        spawned = CellSpawned.new(1,1,2,board.id,board.version+1)
        board.apply(spawned)

        assert_equal 2, board.grid[1][1]
        assert_equal 1, board.version
    end

    def test_apply_cell_moved
        event = BoardCreated.new(5,5,2202,0)
        board = Board.new(board_created=event)

        spawned = CellSpawned.new(1,1,2,board.id, board.version+1)
        board.apply(spawned)

        assert_equal 2, board.grid[1][1]

        moved = CellMoved.new(1,1,1,2,board.id, board.version+1)
        board.apply(moved)

        assert_equal 0, board.grid[1][1]
        assert_equal 2, board.grid[1][2]
        assert_equal 2, board.version
    end

    def test_apply_cells_merged
        event = BoardCreated.new(5,5,2222,0)
        board = Board.new(board_created=event)

        spawned1 = CellSpawned.new(1,1,2,board.id, board.version+1)
        spawned2 = CellSpawned.new(1,2,2,board.id, board.version+2)
        board.apply(spawned1)
        board.apply(spawned2)

        assert_equal 2, board.grid[1][1]
        assert_equal 2, board.grid[1][2]

        merged = CellsMerged.new(1,1,1,2,board.id, board.version+1)
        board.apply(merged)

        assert_equal 0, board.grid[1][1]
        assert_equal 4, board.grid[1][2]
        assert_equal 3, board.version
    end

    def test_apply_board_deactivated
        event = BoardCreated.new(5,5,2222,0)
        board = Board.new(board_created=event)

        assert_true board.is_active

        deactivated = BoardDeactivated.new(board.id, board.version+1)
        board.apply(deactivated)

        assert_false board.is_active
    end

    def test_full_board_should_not_have_empty_cells
        event = BoardCreated.new(2,2,222,0)
        board = Board.new(board_created=event)

        version = board.version
        spawnes = []
        for i in 0..1
            for j in 0..1
                version += 1
                board.apply(CellSpawned.new(i,j,2,board.id,version))
            end
        end

        assert_false board.empty_cells.any?
    end

    def test_empty_cells_should_return_empty_cell_locations
        event = BoardCreated.new(2,2,222,0)
        board = Board.new(board_created=event)

        assert_true board.empty_cells.any?
        assert_equal 4, board.empty_cells.length

        board.apply(CellSpawned.new(1,1,2,board.id, board.version+1))

        assert_equal 3, board.empty_cells.length
    end

    def test_has_equal_neighbors
        event = BoardCreated.new(2,2,222,0)
        board = Board.new(board_created=event)

        version = board.version
        for i in 0..1
            for j in 0..1
                version += 1
                board.apply(CellSpawned.new(i,j,version,board.id,version))
            end
        end

        assert_false board.has_equal_neighbors?

        board.apply(CellSpawned.new(0,0,2,board.id, version+1))

        assert_true board.has_equal_neighbors?
    end
end
