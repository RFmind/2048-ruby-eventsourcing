require 'test/unit'
require_relative '../../src/core/merge.rb'

class TestUtils < Test::Unit::TestCase

    def test_deepcopy
        coll = [
            [0,1,2],
            [3,4,5]
        ]
        copy = deepcopy(coll, false)

        assert_equal coll[1][1], copy[1][1]
        
        copy[1][1] = 9
        assert_not_equal coll[1][1], copy[1][1]
    end

    def test_pair
        i1,j1 = 1,1
        i2,j2 = 2,1

        assert_equal [[i1,j1], [i2,j2]], pair(i1,j1,i2,j2,false)
        assert_equal [[j1,i1], [j2,i2]], pair(i1,j1,i2,j2,true)
    end

    def test_ranges
        outer_max = 4
        inner_max = 5
        reverse = false

        outer, inner = ranges(outer_max, inner_max, reverse)
        assert_equal [0,1,2,3], outer.take(4)
        assert_equal [1,2,3,4], inner.take(4)

        reverse = true
        outer, inner = ranges(outer_max, inner_max, reverse)
        assert_equal [3,2,1,0], outer.take(4)
        assert_equal [3,2,1,0], inner.take(4)
    end

    def test_merge_moves_example
        grid = Array.new(5) { Array.new(5, 0) }
        grid[1][1] = 2
        grid[2][3] = 4

        #   0 0 0 0 0
        #   0 2 0 0 0
        #   0 0 0 4 0
        #   0 0 0 0 0
        
        # merge left
        actions = merge(grid, false, false)

        assert_equal 2, actions.length
        assert_equal 2, actions.select { |a| a.include?(:move) }.length
        assert_equal 0, actions.select { |a| a.include?(:merge) }.length

        assert_equal [1,1], actions[0][0]  # move1
        assert_equal [1,0], actions[0][1]
        assert_equal :move, actions[0][2]

        assert_equal [2,3], actions[1][0]  # move2
        assert_equal [2,0], actions[1][1]
        assert_equal :move, actions[1][2]

        # merge right
        actions = merge(grid, false, true)
        assert_equal 2, actions.length
        assert_equal 2, actions.select { |a| a.include?(:move) }.length
        assert_equal 0, actions.select { |a| a.include?(:merge) }.length

        assert_equal [2,3], actions[0][0]    # move1
        assert_equal [2,4], actions[0][1]
        assert_equal :move, actions[0][2] 
        
        assert_equal [1,1], actions[1][0]    # move2
        assert_equal [1,4], actions[1][1]
        assert_equal :move, actions[1][2]
        
        # merge up
        actions = merge(grid, true, false)
        assert_equal 2, actions.length
        assert_equal 2, actions.select { |a| a.include?(:move) }.length
        assert_equal 0, actions.select { |a| a.include?(:merge) }.length
        
        assert_equal [1,1], actions[0][0]  # move1
        assert_equal [0,1], actions[0][1]
        assert_equal :move, actions[0][2]

        assert_equal [2,3], actions[1][0]  # move2
        assert_equal [0,3], actions[1][1]
        assert_equal :move, actions[1][2]

        # merge down
        actions = merge(grid, true, true)
        assert_equal 2, actions.length
        assert_equal 2, actions.select { |a| a.include?(:move) }.length
        assert_equal 0, actions.select { |a| a.include?(:merge) }.length
        
        assert_equal [2,3], actions[0][0]  # move1
        assert_equal [4,3], actions[0][1]
        assert_equal :move, actions[0][2]

        assert_equal [1,1], actions[1][0]  # move2
        assert_equal [4,1], actions[1][1]
        assert_equal :move, actions[1][2]
    end

    def test_merge_example2
        grid = Array.new(5) { Array.new(5, 0) }
        grid[1][1] = 2
        grid[2][1] = 2

        #   0 0 0 0 0
        #   0 2 0 0 0
        #   0 2 0 0 0
        #   0 0 0 0 0
        
        # merge left
        actions = merge(grid, false, false)
        assert_equal 2, actions.length
        assert_equal 2, actions.select { |a| a.include?(:move) }.length
        assert_equal 0, actions.select { |a| a.include?(:merge) }.length

        assert_equal [1,1], actions[0][0]  # move1
        assert_equal [1,0], actions[0][1]
        assert_equal :move, actions[0][2]

        assert_equal [2,1], actions[1][0]  # move2
        assert_equal [2,0], actions[1][1]
        assert_equal :move, actions[1][2]

        # merge right
        actions = merge(grid, false, true)
        assert_equal 2, actions.length
        assert_equal 2, actions.select { |a| a.include?(:move) }.length
        assert_equal 0, actions.select { |a| a.include?(:merge) }.length

        assert_equal [2,1], actions[0][0]  # move1
        assert_equal [2,4], actions[0][1]
        assert_equal :move, actions[0][2]

        assert_equal [1,1], actions[1][0]  # move2
        assert_equal [1,4], actions[1][1]
        assert_equal :move, actions[1][2]
        
        # merge up
        actions = merge(grid, true, false)
        assert_equal 2, actions.length
        assert_equal 1, actions.select { |a| a.include?(:move) }.length
        assert_equal 1, actions.select { |a| a.include?(:merge) }.length

        assert_equal [1,1], actions[0][0]  # move
        assert_equal [0,1], actions[0][1]
        assert_equal :move, actions[0][2]

        assert_equal [2,1], actions[1][0]  # merge
        assert_equal [0,1], actions[1][1]
        assert_equal :merge, actions[1][2]

        # merge down
        actions = merge(grid, true, true)
        assert_equal 2, actions.length
        assert_equal 1, actions.select { |a| a.include?(:move) }.length
        assert_equal 1, actions.select { |a| a.include?(:merge) }.length

        assert_equal [2,1], actions[0][0]  # move
        assert_equal [4,1], actions[0][1]
        assert_equal :move, actions[0][2]

        assert_equal [1,1], actions[1][0]  # merge
        assert_equal [4,1], actions[1][1]
        assert_equal :merge, actions[1][2]
    end

    def test_merge_full_board_possible_horizontal_merge
        grid = [
            [16, 32],
            [4, 4]
        ]
        
        # merge left
        actions = merge(grid, false, false)
        assert_equal 1, actions.length
        assert_equal 0, actions.select { |a| a.include?(:move) }.length
        assert_equal 1, actions.select { |a| a.include?(:merge) }.length

        assert_equal [1,1], actions[0][0]  # merge
        assert_equal [1,0], actions[0][1]
        assert_equal :merge, actions[0][2]

        # merge right
        actions = merge(grid, false, true)
        assert_equal 1, actions.length
        assert_equal 0, actions.select { |a| a.include?(:move) }.length
        assert_equal 1, actions.select { |a| a.include?(:merge) }.length

        assert_equal [1,0], actions[0][0]  # merge
        assert_equal [1,1], actions[0][1]
        assert_equal :merge, actions[0][2]
        
        # merge up
        actions = merge(grid, true, false)
        assert_equal 0, actions.length
        assert_equal 0, actions.select { |a| a.include?(:move) }.length
        assert_equal 0, actions.select { |a| a.include?(:merge) }.length

        # merge down
        actions = merge(grid, true, true)
        assert_equal 0, actions.length
        assert_equal 0, actions.select { |a| a.include?(:move) }.length
        assert_equal 0, actions.select { |a| a.include?(:merge) }.length
    end

    def test_merge4
        grid = [
            [2, 0, 2],
            [2, 0, 4],
            [2, 4, 8]
        ]

        # merge left
        actions = merge(grid, false, false)
        assert_equal 2, actions.length
        assert_equal 1, actions.select { |a| a.include?(:move) }.length
        assert_equal 1, actions.select { |a| a.include?(:merge) }.length

        assert_equal [0,2], actions[0][0]  # merge
        assert_equal [0,0], actions[0][1]
        assert_equal :merge, actions[0][2]

        assert_equal [1,2], actions[1][0]  # move
        assert_equal [1,1], actions[1][1]
        assert_equal :move, actions[1][2]
    end
end
