require_relative '../../src/core/handlers.rb'
require_relative '../../src/core/board.rb'
require_relative '../../src/core/merge.rb'
require 'test/unit'

class TestHandlers < Test::Unit::TestCase

    def test_create_board
        events = create_board(4, 5)

        board_created = events.first
        assert_not_nil board_created.aggregate_id
        assert_equal 0, board_created.aggregate_version
        assert_equal 4, board_created.payload[:width]
        assert_equal 5, board_created.payload[:height]
    end

    def test_move
        create_events = create_board(4, 4)

        assert_equal 3, create_events.length
        assert_true create_events[0].is_a? BoardCreated
        assert_true create_events[1].is_a? CellSpawned
        assert_true create_events[2].is_a? CellSpawned

        board = replicate_board(create_events)
        move_events = move(board, "Right")

        assert_true move_events.pop.is_a? CellSpawned

        if create_events[1].payload[:x] != create_events[2].payload[:x]
            assert_equal 0, move_events.select{|e| e.is_a?(CellsMerged)}.length
            assert_equal move_events.length, move_events.select{|e| e.is_a?(CellMoved)}.length
        elsif create_events[1].payload[:value] == create_events[2].payload[:value]
            assert_equal 1, move_events.select{|e| e.is_a?(CellsMerged)}.length
            assert_equal move_events.length-1, move_events.select{|e| e.is_a?(CellMoved)}.length
        end
    end

    def test_board_deactivation
        grid = [
            [16, 32],
            [4, 4]
        ]

        board = mock_board(grid)
        events = move(board, "Left")

        assert_true events.pop.is_a? BoardDeactivated
        assert_true events.pop.is_a? CellSpawned
        assert_true events.pop.is_a? CellsMerged
        assert_equal 0, events.length
    end
    
    def mock_board(grid)
        id = 2245
        version = 0
        events = []
        events.push(BoardCreated.new(grid.length, grid[0].length, id, version))
        version += 1

        for x in 0..grid.length-1
            for y in 0..grid[0].length-1
                events.push(
                    CellSpawned.new(x, y, grid[x][y], id, version))
                version += 1
            end
        end

        return replicate_board(events)
    end

    def replicate_board(events)
        board = nil
        events.each do |ev|
            if board
                board.apply ev
            else
                board = Board.new(board_created=ev)
            end
        end

        return board
    end
end

