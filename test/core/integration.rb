require_relative '../../src/core/handlers.rb'
require_relative '../../src/core/events.rb'
require_relative '../../src/core/board.rb'
require 'test/unit'

class IntegrationTests < Test::Unit::TestCase

    def test_simulation1
        events = create_board(4,4)

        board = nil
        for ev in events
            if board == nil
                board = Board.new(board_created=ev)
            else
                board.apply(ev)
                x = ev.payload[:x]
                y = ev.payload[:y]
                assert_equal ev.payload[:value], board.grid[x][y]
            end
        end

        assert_equal 14, board.empty_cells.length

        events = move(board, 'left')
        assert_true board.grid != nil

        for ev in events

            if ev.is_a?(CellMoved)
                x1 = ev.payload[:x1]
                x2 = ev.payload[:x2]
                y1 = ev.payload[:y1]
                y2 = ev.payload[:y2]
                assert_not_nil x1
                assert_not_nil x2
                assert_not_nil y1
                assert_not_nil y2

                old_to = board.grid[x2][y2]
                old_from = board.grid[x1][y1]
                
                board.apply(ev)
                
                new_to = board.grid[x2][y2]
                new_from = board.grid[x1][y1]
                
                assert_equal 0, old_to
                assert_equal old_from, new_to
                assert_equal 0, new_from

            elsif ev.is_a?(CellsMerged)
                x1 = ev.payload[:x1]
                x2 = ev.payload[:x2]
                y1 = ev.payload[:y1]
                y2 = ev.payload[:y2]
                assert_not_nil x1
                assert_not_nil x2
                assert_not_nil y1
                assert_not_nil y2
                
                old_to = board.grid[x2][y2]
                old_from = board.grid[x1][y1]
                
                board.apply(ev)
                
                new_to = board.grid[x2][y2]
                new_from = board.grid[x1][y1]
                assert_equal old_from, old_to
                assert_equal old_from+old_to, new_to
                assert_equal 0, new_from

            elsif ev.is_a?(CellSpawned)
                x = ev.payload[:x]
                y = ev.payload[:y]
                assert_not_nil x
                assert_not_nil y

                old_value = board.grid[x][y]
                board.apply(ev)
                new_value = board.grid[x][y]

                assert_equal 0, old_value
                assert_not_equal 0, new_value

            elsif ev.is_a?(BoardDeactivated)
                board.apply(ev)
            end
        end
    end
end
