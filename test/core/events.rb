require_relative '../../src/core/events.rb'
require 'test/unit'

class TestEvents < Test::Unit::TestCase

    def test_event_should_have_correct_properties
        ev = Event.new("", 1, 0)

        assert_respond_to ev, "eventid"
        assert_respond_to ev, "aggregate_id"
        assert_respond_to ev, "aggregate_version"
        assert_respond_to ev, "timestamp"
        assert_respond_to ev, "payload"
    end
end
